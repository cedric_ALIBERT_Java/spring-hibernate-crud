package fr.cedricalibert.springdemo.aspect;

import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CRMLoggingAspect {
	//setup logger
	private Logger myLogger = Logger.getLogger(getClass().getName());
	
	//setup pointcut decoration
	@Pointcut("execution(* fr.cedricalibert.springdemo.controller.*.*(..))")
	private void forControllerPackage() {}
	
	@Pointcut("execution(* fr.cedricalibert.springdemo.service.*.*(..))")
	private void forServicePackage() {}
	
	@Pointcut("execution(* fr.cedricalibert.springdemo.dao.*.*(..))")
	private void forDaoPackage() {}
	
	@Pointcut("forControllerPackage() || forServicePackage() || forDaoPackage()")
	private void forAppFlow() {}
	
	// add @Before advice
	@Before("forAppFlow()")
	public void before(JoinPoint joinPoint) {
		// display the method
		String methSign = joinPoint.getSignature().toShortString();
		
		myLogger.info("\n=======> in @Before: calling method: "+methSign);
		
		//display arguments
		Object[] args = joinPoint.getArgs();
		for(Object arg : args) {
			myLogger.info("\n=======> Argument: "+arg);
		}
		
	}
	
	//add @afterReturning Advice
	@AfterReturning(
			pointcut="forAppFlow()",
			returning="result")
	public void after(JoinPoint joinPoint, Object result) {
		// display the method
		String methSign = joinPoint.getSignature().toShortString();
		
		myLogger.info("\n=======> in @AfterReturning: calling method: "+methSign);
		
		//display data return
		myLogger.info("\n=======> Data return: "+result);
		
	}
}
